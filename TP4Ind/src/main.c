#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

int mapa[7][3];
int ciudades[6];
int mapaEntry=0;


void inicializarMapa();
void insertDataMap (int, int, int);
void showMapa();
void inicializarCiudades();
int listar_ciudades(int ciudad_actual);
void mostrar_ciudad(int ciudad);
int viajar(int ciudad_actual, int ciudad_destino);

int main(int argc, char **argv) {
	inicializarMapa();
	inicializarCiudades();
	int ciudad_actual=-1;
	int ciudad_destino=-1;
	int resp;
	int work = 1;//true
	int kilometros = 0;
	while (work){
		ciudad_destino = listar_ciudades(ciudad_actual);
		kilometros+=viajar(ciudad_actual, ciudad_destino);
		printf("Kms recc %d \n", kilometros);
		ciudad_actual=ciudad_destino;
		printf("Desea continuar? 1-Si 0-No\n");
		scanf("%d", &resp);
		if (resp != 1){
			work=0;
		}
	}
	printf("Cantidad de kms viajados: %d \n", kilometros);
}

int viajar(int ciudad_actual, int ciudad_destino){
	int distancia=0;
	for (int i =0; i<6;i++){
		if(mapa[i][0]==ciudad_actual && mapa[i][1]==ciudad_destino){
			distancia=mapa[i][2];
		}else if(mapa[i][0]==ciudad_destino && mapa[i][1]==ciudad_actual){
			distancia=mapa[i][2];
		}
	}
	/* printf("Viaje desde: %d a %d recorriendo %d \n", ciudad_actual, ciudad_destino, distancia);  */
	return distancia;
}

int listar_ciudades(int ciudad_actual){
	int tmp;
	printf("Seleccione el numero de una de las ciudades\n");
	for (int i=0; i<6; i++){
		if (ciudad_actual == ciudades[i]){
			i=i+1;
		}else{
			mostrar_ciudad(i);
		}
	}
	scanf("%d", &tmp);
	return tmp;
}

void mostrar_ciudad(int ciudad){
	switch(ciudad){
	case 0:
		printf("0- SanJuan \n");
		break;
	case 1:
		printf("1- Mendoza \n");
		break;
	case 2:
		printf("2- SanRafael \n");
		break;
	case 3:
		printf("3- SanLuis \n");
		break;
	case 4:
		printf("4- RioCuarto \n");
		break;
	case 5:
		printf("5- Cordoba \n");
		break;
	default:
		printf("error\n");
	}
}

void inicializarCiudades(){
	for(int i =0; i<6;i++){
		ciudades[i]=i;
	}
}

void inicializarMapa(){
	insertDataMap(0, 1, 170);
	insertDataMap(0, 5, 584);
	insertDataMap(1, 2, 233);
	insertDataMap(1, 3, 259);
	insertDataMap(2, 3, 275);
	insertDataMap(3, 4, 216);
	insertDataMap(4, 5, 214);
}

void insertDataMap (int org, int dest, int dist){
	mapa[mapaEntry][0]=org;
	mapa[mapaEntry][1]=dest;
	mapa[mapaEntry][2]=dist;
	mapaEntry++;
}


