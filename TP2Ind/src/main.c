#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <limits.h> 

#define EMPTY 1
#define OBSTACLE -9 
#define POSITION 10
#define END 0

int board[10][10];
int termalBoard[10][10];
int exitX;
int exitY;
int actualX;
int actualY;

typedef struct {
	int y;
	int spaces;
}posibility;

void initialiceBoard();
void putObstacles();
void showBoard();
void defineStartAndEnd();
void initialiceTermal();
void showTermal();
void move();

int main(int argc, char **argv) {

	initialiceBoard();//crear tabla y agrega obstaculos
	putObstacles();
	defineStartAndEnd();
	showBoard();
	initialiceTermal();
	showTermal();
	//Comenzar a verificar
	while (termalBoard[actualX][actualY] != 99){
		move();
	}
	return 0;
}

void initialiceBoard() {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			board[i][j] = EMPTY;
		}
	}
}

void putObstacles(){
	int x, y;
	printf("Obstacles:\n");
	for (int k=0; k<8;k++){
		srand(time(NULL));
		x = rand() % (10);
		sleep(1);
		srand(time(NULL));
		y = rand() % (10);
		printf ("x: %d y: %d \n",x,y);
		if (board[x][y] == EMPTY){
			board[x][y] = OBSTACLE;
		}else{
			k--;
		}
	}
}

void defineStartAndEnd(){
	int x, y;
	int flag=0;
	printf("Start and End:\n");
	for (int k=0; k<2;k++){
		srand(time(NULL));
		x = rand() % (10);
		sleep(1);
		if (x != 0 || x !=9){
			srand(time(NULL));
			y = rand() % (2);
			if (y){
				y=9;	
			}else{
				y=0;	
			};
		}else{
			srand(time(NULL));
			y = rand() % (10);
		}
		printf ("x: %d y: %d \n",x,y);
		if (board[x][y] == EMPTY && flag==0){
			board[x][y] = POSITION;
			flag++;
			actualY=y;
			actualX=x;
		}else if (board[x][y] == EMPTY && flag==1){
			board[x][y] = END;
			exitX = x;
			exitY = y;
		}else{
			k--;
		}
		sleep(1);
	}
}

void showBoard() {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (board[i][j] == OBSTACLE){
			printf("X ");
			}else{
			printf("%d ", board[i][j]);
			}
			if (j == 9)
				printf("\n");
		}
	}
}

void initialiceTermal()  {
	int raiz=1;
	printf("Termal para %d %d:\n", exitX, exitY);
	for (int i = 0; i < 10; ++i) {
		for (int j = 0; j < 10; ++j) {
			if(board [i][j] != OBSTACLE && board [i][j] != POSITION){
			termalBoard [i][j] = 99;
			raiz=sqrt((((exitX - i) * (exitX - i)) + (exitY - j) * (exitY - j)));
				termalBoard [i][j] -= roundf(raiz);
			}else{
				termalBoard [i][j]=board [i][j];
			}
		}
	}	
}
void showTermal() {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			printf("%d ", termalBoard[i][j]);
			if (j == 9)
				printf("\n");
		}
	}
}

void move(){
	int cost1=-20;//Siempre menor
	int cost2=-20;
	int x1, x2, y1, y2;

	printf("Posición actual %d %d \n", actualX, actualY);
	//Ver arriba
	if (actualX !=0){//OutOfBonds
		cost1 = termalBoard[actualX-1][actualY];
		x1=actualX-1;
		y1=actualY;
		printf("En %d %d costo de %d\n", x1, y1, cost1);
	}
	//Ver Izq 
	if (actualY !=0){
		cost2 = termalBoard[actualX][actualY-1];
		x2=actualX;
		y2=actualY-1;
		printf("En %d %d costo de %d\n", x2, y2, cost2);
	}
	//Comparo ambos
	if (cost1<cost2){
		cost1=cost2;
		x1=x2;
		y1=y2;
	}
	
	//Ver Down
	if (actualX !=9){
		cost2 = termalBoard[actualX+1][actualY];
		x2=actualX+1;
		y2=actualY;
		printf("En %d %d costo de %d\n", x2, y2, cost2);
	}
	if (cost1<cost2){
		cost1=cost2;
		x1=x2;
		y1=y2;
	}
	//Ver Rig
	if (actualY !=9){
		cost2 = termalBoard[actualX][actualY+1];
		x2=actualX;
		y2=actualY+1;
		printf("En %d %d costo de %d\n", x2, y2, cost2);
	}
	if (cost1<cost2){
		cost1=cost2;
		x1=x2;
		y1=y2;

	}
	termalBoard[actualX][actualY]-=10; //Se resta prioridad
	board[actualX][actualY]=3;
	actualX=x1;
	actualY=y1;
	printf("Se movio a %d %d\n", actualX, actualY);
	sleep(2);
}
